from rest_framework import serializers
from core.models import Student, Curator, Group


class StudentSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    age = serializers.IntegerField()
    group = serializers.SlugRelatedField(queryset=Group.objects.all(), slug_field='name')

    class Meta:
        model = Student
        fields = 'name', 'age', 'group'


class GroupSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    students = serializers.SerializerMethodField()

    def get_students(self, obj):
        return dict(Student.objects.filter(group=obj).values_list('name', 'age'))

    class Meta:
        model = Group
        fields = 'name', 'students'


class CuratorSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    group = serializers.SlugRelatedField(queryset=Group.objects.all(), slug_field='name', many=True)

    class Meta:
        model = Curator
        fields = 'name', 'email', 'group'
