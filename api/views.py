from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from core.models import Student, Group, Curator
from django.http import HttpResponse
import csv
from api.serializers import StudentSerializer, GroupSerializer, CuratorSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class ExportStudentsCVS(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Students.csv"'
        writer = csv.writer(response)
        students = Student.objects.all().order_by('group')
        for student in students:
            writer.writerow([student.group.name, student.name, str(student.age)])
        return response


class StudentView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class GroupView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CuratorView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Curator.objects.all()
    serializer_class = CuratorSerializer
