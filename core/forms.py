from django import forms
from core.models import Group, Student, Curator


class GroupForm(forms.Form):
    name = forms.CharField(max_length=255)

    def save(self):
        Group.objects.create(
            **self.cleaned_data,
        )


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = "__all__"
        widgets = {
            'name': forms.widgets.TextInput(attrs={'class': 'form-input'}),
            'age': forms.widgets.NumberInput(),
            'group': forms.widgets.RadioSelect()
        }
