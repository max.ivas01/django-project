from django.test import TestCase
from core.views import IndexView, CuratorsListView, GroupsListView, AddGroupView, UpdateGroupView, AddStudentView,\
    UpdateStudentView
from core.models import Student, Group
from django.db.models.aggregates import Count, Avg, Max, Min


class DiagnosticTestCase(TestCase):

    # SET UP FOR UPDATE TESTS

    def setUp(self):
        super(DiagnosticTestCase, self).setUp()
        Group.objects.create(name='TestGroup')
        Student.objects.create(name="TestStudent", age='404', group=Group.objects.get(id=1))

    # INDEX VIEW TESTS

    def test_index_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_template_name(self):
        view = IndexView()
        self.assertEqual(view.template_name, 'index.html')

    # CURATORS LIST VIEW TESTS

    def test_curators_url(self):
        response = self.client.get('/curators/')
        self.assertEqual(response.status_code, 200)

    def test_curators_template_name(self):
        view = GroupsListView()
        self.assertEqual(view.template_name, 'groups_list.html')

    # GROUPS LIST VIEW TESTS

    def test_groups_url(self):
        response = self.client.get('/groups/')
        self.assertEqual(response.status_code, 200)

    def test_groups_template_name(self):
        view = CuratorsListView()
        self.assertEqual(view.template_name, 'curators_list.html')

    def test_groups_context(self):
        groups = Group.objects.annotate(
            stud_amount=Count('students'),
            avg_age=Avg('students__age'),
            max_age=Max('students__age'),
            min_age=Min('students__age')).prefetch_related('curator')
        response = self.client.get('/groups/')
        self.assertQuerysetEqual(response.context['groups'], groups, transform=lambda x: x)

    # ADD GROUPS VIEW TESTS

    def test_add_group_url(self):
        response = self.client.get('/add/group')
        self.assertEqual(response.status_code, 200)

    def test_add_group_template_name(self):
        view = AddGroupView()
        self.assertEqual(view.template_name, 'add_something.html')

    def test_add_group_context(self):
        response = self.client.get('/add/group')
        self.assertEqual(response.context['name'], 'Group')

    # ADD STUDENT VIEW TESTS

    def test_add_student_url(self):
        response = self.client.get('/add/student')
        self.assertEqual(response.status_code, 200)

    def test_add_student_template_name(self):
        view = AddStudentView()
        self.assertEqual(view.template_name, 'add_something.html')

    def test_add_student_context(self):
        response = self.client.post('/add/student')
        self.assertEqual(response.context['name'], 'Student')

    # UPDATE GROUP VIEW TESTS

    def test_update_group_url(self):
        response = self.client.get('/update/group/1')
        self.assertEqual(response.status_code, 200)

    def test_update_group_template_name(self):
        view = UpdateGroupView()
        self.assertEqual(view.template_name, 'update_something.html')

    def test_update_group_context(self):
        response = self.client.get('/update/group/1')
        self.assertEqual(response.context['name'], 'Group')

    # UPDATE STUDENT VIEW TESTS

    def test_update_student_url(self):
        response = self.client.get('/update/student/1')
        self.assertEqual(response.status_code, 200)

    def test_update_student_template_name(self):
        view = UpdateStudentView()
        self.assertEqual(view.template_name, 'update_something.html')

    def test_update_student_context(self):
        response = self.client.get('/update/student/1')
        self.assertEqual(response.context['name'], 'Student')
