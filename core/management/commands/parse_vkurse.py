from django.core.management.base import BaseCommand
import requests
from core.models import CurrencyParser
from datetime import datetime
import pytz


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get('http://vkurse.dp.ua/course.json')
        print('Status ', response.status_code)

        if response.status_code == 200:
            currencies = response.json()

            CurrencyParser.objects.create(
                source='Vkurse',
                date=datetime.now(tz=pytz.utc).replace(microsecond=0),
                dollar=f"{currencies['Dollar']['buy']} / {currencies['Dollar']['sale']}",
                euro=f"{currencies['Euro']['buy']} / {currencies['Euro']['sale']}",
                rubles=f"{currencies['Rub']['buy']} / {currencies['Rub']['sale']}",
            )

            print('Parsed successfully')
        else:
            print('Parsing error')
