from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import CurrencyParser
from datetime import datetime
import pytz


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get('https://minfin.com.ua/currency/')
        print('Status ', response.status_code)

        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currencies = []

            for tr in tree.xpath('/html/body/main/div/div/div[1]/div/section[2]/div/table[1]/tbody')[0]:
                for td in tr:
                    currencies.append(td.text_content().split())

            CurrencyParser.objects.create(
                source='Minfin',
                date=datetime.now(tz=pytz.utc).replace(microsecond=0),
                dollar=f'{currencies[1][0]} / {currencies[1][4]}',
                euro=f'{currencies[5][0]} / {currencies[5][4]}',
                rubles=f'{currencies[9][0]} / {currencies[9][4]}',
            )

            print('Parsed successfully')
        else:
            print('Parsing error')
