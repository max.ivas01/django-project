from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import CurrencyParser
from datetime import datetime
import pytz


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get('https://finance.i.ua/')
        print('Status ', response.status_code)

        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currencies = []

            for tr in tree.xpath('/html/body/div[3]/div[3]/div/div[1]/div[1]/div[1]/div/table/tbody')[0]:
                for td in tr:
                    currencies.append(td.text_content()[:-6])

            CurrencyParser.objects.create(
                source='Fiance',
                date=datetime.now(tz=pytz.utc).replace(microsecond=0),
                dollar=f'{currencies[1]} / {currencies[2]}',
                euro=f'{currencies[5]} / {currencies[6]}',
                rubles=f'{currencies[9]} / {currencies[10]}',
            )

            print('Parsed successfully')
        else:
            print('Parsing error')
