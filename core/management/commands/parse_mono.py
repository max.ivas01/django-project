from django.core.management.base import BaseCommand
import requests
from core.models import CurrencyParser
from datetime import datetime
import pytz


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get('https://api.monobank.ua/bank/currency')
        print('Status ', response.status_code)

        if response.status_code == 200:
            currencies = response.json()

            CurrencyParser.objects.create(
                source='MonoBank',
                date=datetime.fromtimestamp(response.json()[0]['date'], pytz.utc),
                dollar=f'{currencies[0]["rateBuy"]} / {currencies[0]["rateSell"]}',
                euro=f'{currencies[1]["rateBuy"]} / {currencies[1]["rateSell"]}',
                rubles=f'{currencies[2]["rateBuy"]} / {currencies[2]["rateSell"]}',
            )

            print('Parsed successfully')
        else:
            print('Parsing error')
