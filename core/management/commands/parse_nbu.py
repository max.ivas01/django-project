from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import CurrencyParser
from datetime import datetime
import pytz


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get('https://bank.gov.ua/ua/markets/exchangerates')
        print('Status ', response.status_code)

        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currencies = []

            for tr in tree.xpath('/html/body/main/div/div/div/div[1]/div[2]/div[1]/div[2]/div[3]/div/table/tbody')[0]:
                for td in tr:
                    currencies.append(td.text_content().split())

            CurrencyParser.objects.create(
                source='NBU',
                date=datetime.now(tz=pytz.utc).replace(microsecond=0),
                dollar=currencies[currencies.index(["USD"])+3][0],
                euro=currencies[currencies.index(["EUR"])+3][0],
                rubles=currencies[currencies.index(["RUB"])+3][0],
            )

            print('Parsed successfully')
        else:
            print('Parsing error')
