# Generated by Django 3.1.7 on 2021-05-08 09:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_contactus_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='CurrencyParser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source', models.CharField(max_length=255)),
                ('date', models.CharField(max_length=255)),
                ('dollar', models.CharField(max_length=255)),
                ('euro', models.CharField(max_length=255)),
                ('rubles', models.CharField(max_length=255)),
            ],
        ),
    ]
