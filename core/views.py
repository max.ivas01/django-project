from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, FormView
from core.models import Group, Student, Curator, ContactUs
from django.db.models.aggregates import Count, Avg, Max, Min
from core.forms import StudentForm, GroupForm


class IndexView(TemplateView):
    template_name = 'index.html'


class GroupsListView(ListView):
    template_name = 'groups_list.html'
    model = Group

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(GroupsListView, self).get_context_data(**kwargs)
        groups = Group.objects.annotate(
            stud_amount=Count('students'),
            avg_age=Avg('students__age'),
            max_age=Max('students__age'),
            min_age=Min('students__age')).prefetch_related('curator')
        context['groups'] = groups
        return context


class CuratorsListView(ListView):
    template_name = 'curators_list.html'
    model = Curator
    context_object_name = 'curators'


class UpdateGroupView(UpdateView):
    template_name = 'update_something.html'
    success_url = '/'
    model = Group
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(UpdateGroupView, self).get_context_data(**kwargs)
        context['name'] = 'Group'
        return context


class UpdateStudentView(UpdateView):
    template_name = 'update_something.html'
    success_url = '/'
    model = Student
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(UpdateStudentView, self).get_context_data(**kwargs)
        context['name'] = 'Student'
        return context


class AddStudentView(TemplateView):
    template_name = 'add_something.html'

    def get_context_data(self, **kwargs):
        context = super(AddStudentView, self).get_context_data(**kwargs)
        context['form'] = StudentForm()
        context['name'] = 'Student'
        return context

    def post(self, request):
        form = StudentForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)


class AddGroupView(FormView):
    template_name = 'add_something.html'
    form_class = GroupForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(AddGroupView, self).get_context_data(**kwargs)
        context['form'] = GroupForm()
        context['name'] = 'Group'
        return context

    def form_valid(self, form):
        form.save()
        return super(AddGroupView, self).form_valid(form)


class ContactUsView(CreateView):
    template_name = "contact_us.html"
    success_url = '/contact_us/done'
    model = ContactUs
    fields = '__all__'

