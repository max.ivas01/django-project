from django.db import models
from core.tasks import contact_us_send_email


class Group(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='students')

    def __str__(self):
        return self.name


class Curator(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    group = models.ManyToManyField(Group, related_name='curator')

    def __str__(self):
        return self.name


class Logger(models.Model):
    date = models.DateTimeField(default=None)
    path = models.CharField(max_length=255)
    method = models.CharField(max_length=255)
    execution_time = models.CharField(max_length=255)

    def __str__(self):
        return "LOG " + str(self.date)


class ContactUs(models.Model):
    name = models.CharField(max_length=255, default=None)
    title = models.CharField(max_length=255)
    message = models.TextField()
    email_from = models.EmailField()

    def __str__(self):
        return f"{self.title} (from {self.email_from})"

    def save(self, *args, **kwargs):
        super(ContactUs, self).save(*args, **kwargs)
        contact_us_send_email.delay(self.name, self.title, self.message, self.email_from)


class CurrencyParser(models.Model):
    source = models.CharField(max_length=255)
    date = models.CharField(max_length=255)
    dollar = models.CharField(max_length=255)
    euro = models.CharField(max_length=255)
    rubles = models.CharField(max_length=255)

    def __str__(self):
        return f"[{self.date}] {self.source}"
