from django.utils.deprecation import MiddlewareMixin
from core.models import Logger
import time
from datetime import datetime
import pytz


class LogMiddleware(MiddlewareMixin):

    def process_request(self, request):
        self.start_time = time.time()

    def process_response(self, request, response):
        if not request.path.startswith('/admin'):
            date = datetime.now(tz=pytz.utc)
            execution_time = round(time.time() - self.start_time, 3)
            log = Logger(date=date, path=request.path, method=request.method, execution_time=execution_time)
            log.save()
        return response
