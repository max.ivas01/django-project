from HW5.celery import app
import core.models as models
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


@app.task
def delete_old_logs():
    timing = datetime.now() - timedelta(days=7)
    old_logs = models.Logger.objects.filter(date__lt=timing)
    amount = old_logs.count()
    old_logs.delete()
    print(f'Deleted {amount} old logger records.')


@app.task
def contact_us_send_email(name, title, message, email_from):
    send_mail(f"new message from {name} - {email_from}",
              f'{title}\n{message}',
              settings.SUPPORT_EMAIL_FROM,
              [settings.ADMIN_EMAIL],
              )


@app.task
def generate_new_token():
    Token.objects.all().delete()
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)
    amount = Token.objects.all().count()
    print(f'Generated {amount} new tokens.')
