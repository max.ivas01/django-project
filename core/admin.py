from django.contrib import admin
from core.models import Group, Student, Curator, Logger, ContactUs, CurrencyParser


admin.site.register(Group)
admin.site.register(Student)
admin.site.register(Curator)
admin.site.register(Logger)
admin.site.register(ContactUs)
admin.site.register(CurrencyParser)
