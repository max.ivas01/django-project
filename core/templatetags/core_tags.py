from django import template
from random import choices

register = template.Library()


@register.filter
def even_only(arr):
    even_arr = [x for x in arr if x % 2 == 0]
    return even_arr


@register.filter
def words_amount(string):
    words = string.split()
    return len(words)


@register.inclusion_tag('get_5_tag.html')
def get_5_tag(query):
    cut_query = choices(query, k=5)
    return {'cut_query': cut_query}
