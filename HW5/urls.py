"""HW5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import include, path
from core import views
from api import views as api_views
from rest_framework.routers import SimpleRouter
from rest_framework.authtoken.views import obtain_auth_token

router = SimpleRouter()
router.register('students', api_views.StudentView)
router.register('groups', api_views.GroupView)
router.register('curators', api_views.CuratorView)

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('groups/', views.GroupsListView.as_view(), name='groups'),
    path('curators/', views.CuratorsListView.as_view(), name='curators'),
    path('add/group', views.AddGroupView.as_view(), name='add_group'),
    path('add/student', views.AddStudentView.as_view(), name='add_student'),
    path('update/student/<int:pk>', views.UpdateStudentView.as_view(), name='update_student'),
    path('update/group/<int:pk>', views.UpdateGroupView.as_view(), name='update_group'),
    path('contact_us/', views.ContactUsView.as_view(), name='contact_us'),
    path('contact_us/done/', views.TemplateView.as_view(template_name="contact_us_done.html"), name="contact_us_done"),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/students/download', api_views.ExportStudentsCVS.as_view(), name='export_students_to_csv'),
    path('api/', include(router.urls)),
    path('api/obtain-token/', obtain_auth_token),

    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),
]
